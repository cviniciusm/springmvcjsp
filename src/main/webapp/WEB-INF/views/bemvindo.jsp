<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link rel="stylesheet" href="/css/estilo.css">
</head>
<body>
<div>
    <span id="essespan">span</span>${usuario.nome}
</div>
<div>
    <img class="estiloImagem" src="/images/PorDoSol.jpg">
</div>
<div>
    <c:forEach items="${continentes}" var="continente">
    <p>${continente}</p>
    </c:forEach>
</div>
<script src="/js/jquery/jquery-3.6.0.min.js"></script>
<script src="/js/alteraspan.js"></script>
</body>
</html>